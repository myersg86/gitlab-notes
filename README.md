# Interview Notes



## Technical Interview

## Links

https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics

https://about.gitlab.com/handbook/support/workflows/support-engineering/ldap/debugging_ldap.html

https://docs.gitlab.com/ee/administration/auth/ldap.html#troubleshooting

https://docs.gitlab.com/ee/administration/auth/how_to_configure_ldap_gitlab_ce/

https://docs.gitlab.com/ee/administration/auth/ldap.html#configuration

https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics

### Docs

https://docs.gitlab.com/ee/administration/

https://docs.gitlab.com/ee/user/

https://docs.gitlab.com/runner/

### Install/Update

https://about.gitlab.com/install/

https://docs.gitlab.com/omnibus/manual_install.html

https://docs.gitlab.com/ce/install/requirements.html

https://about.gitlab.com/update/

### Configure

https://docs.gitlab.com/omnibus/#configuring

https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure

https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab

https://docs.gitlab.com/omnibus/settings/nginx.html#enable-https

https://docs.gitlab.com/omnibus/settings/smtp.html#smtp-settings

https://docs.gitlab.com/ce/administration/monitoring/prometheus/index.html

https://docs.gitlab.com/omnibus/roles/README.html

https://docs.gitlab.com/ee/ci/quick_start/

https://docs.gitlab.com/omnibus/settings/database.html

### Maintenence/Troubleshooting

https://docs.gitlab.com/omnibus/maintenance/

https://docs.gitlab.com/omnibus/settings/logs.html

https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure

### Support

https://about.gitlab.com/get-help/#technical-support
https://about.gitlab.com/support/#premium-support

### Issues:

https://gitlab.com/gitlab-org/gitlab-ce/issues
https://gitlab.com/gitlab-org/gitlab-ee/issues

Commands:

`sudo gitlab-ctl status`

`sudo gitlab-ctl start`

`sudo gitlab-ctl stop`

`sudo gitlab-ctl restart`





### Debug GitLab CE Errors

### Log locations

To debug Gitlab errors, check the following files depending on the issue:

**Gitlab log:** the Gitlab log is at `/opt/gitlab/embedded/service/gitlab-rails/log/production.log`

**Sidekiq log:** check it if the Sidekiq server cannot be started at `/var/log/gitlab/gitlab-rails/sidekiq.log`

**Gitlab-shell log:** the Gitlab-shell log is at `/var/log/gitlab/gitlab-shell/gitlab-shell.log`

To debug NGINX errors, check the NGINX log file at `/var/log/gitlab/nginx/gitlab_error.log`

### edge cases/different software

[gitlab-recipes > web-server](https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server)

[GitLab recipes: unofficial guides for using GitLab with different software](https://gitlab.com/gitlab-org/gitlab-recipes)

## Omnibus installation

https://docs.gitlab.com/omnibus/README.html

[Installation and Configuration using omnibus package](https://docs.gitlab.com/omnibus/#installation-and-configuration-using-omnibus-package#installation-and-configuration-using-omnibus-package)

[Install](https://about.gitlab.com/install/)

[Maintenance](https://docs.gitlab.com/omnibus/#maintenance)

[Configuring](https://docs.gitlab.com/omnibus/#configuring)

[Updating](https://docs.gitlab.com/omnibus/#updating)

[Troubleshooting](https://docs.gitlab.com/omnibus/#troubleshooting)



## Docker

[GitLab Docker images](https://docs.gitlab.com/omnibus/docker/README.html)



## Git

https://git-scm.com/docs
https://git-scm.com/docs/git-bisect
https://git-scm.com/docs/git-merge
https://git-scm.com/docs/git-branch
https://git-scm.com/docs/git-checkout
https://git-scm.com/docs/git-mergetool
https://git-scm.com/docs/git-fetch
https://git-scm.com/docs/git-pull
https://git-scm.com/docs/git-push
https://git-scm.com/docs/git-diff
https://git-scm.com/docs/git-log



## Maintenance

- [Get service status](https://docs.gitlab.com/omnibus/maintenance/README.html#get-service-status)
- [Starting and stopping](https://docs.gitlab.com/omnibus/maintenance/README.html#starting-and-stopping)
- [Invoking Rake tasks](https://docs.gitlab.com/omnibus/maintenance/README.html#invoking-rake-tasks)
- [Starting a Rails console session](https://docs.gitlab.com/omnibus/maintenance/README.html#starting-a-rails-console-session)
- [Starting a Postgres superuser psql session](https://docs.gitlab.com/omnibus/maintenance/README.html#starting-a-postgres-superuser-psql-session)
- [Container registry garbage collection](https://docs.gitlab.com/omnibus/maintenance/README.html#container-registry-garbage-collection)

## Configuring

- [Configuring the external url](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab)
- [Configuring a relative URL for Gitlab (experimental)](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-a-relative-url-for-gitlab)
- [Storing git data in an alternative directory](https://docs.gitlab.com/omnibus/settings/configuration.html#storing-git-data-in-an-alternative-directory)
- [Changing the name of the git user group](https://docs.gitlab.com/omnibus/settings/configuration.html#changing-the-name-of-the-git-user-group)
- [Specify numeric user and group identifiers](https://docs.gitlab.com/omnibus/settings/configuration.html#specify-numeric-user-and-group-identifiers)
- [Only start omnibus-gitlab services after a given filesystem is mounted](https://docs.gitlab.com/omnibus/settings/configuration.html#only-start-omnibus-gitlab-services-after-a-given-filesystem-is-mounted)
- [Disable user and group account management](https://docs.gitlab.com/omnibus/settings/configuration.html#disable-user-and-group-account-management)
- [Disable storage directory management](https://docs.gitlab.com/omnibus/settings/configuration.html#disable-storage-directories-management)
- [Configuring Rack attack](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-rack-attack)
- [SMTP](https://docs.gitlab.com/omnibus/settings/smtp.html)
- [NGINX](https://docs.gitlab.com/omnibus/settings/nginx.html)
- [LDAP](https://docs.gitlab.com/omnibus/settings/ldap.html)
- [Unicorn](https://docs.gitlab.com/omnibus/settings/unicorn.html)
- [Redis](https://docs.gitlab.com/omnibus/settings/redis.html)
- [Logs](https://docs.gitlab.com/omnibus/settings/logs.html)
- [Database](https://docs.gitlab.com/omnibus/settings/database.html)
- [Reply by email](https://docs.gitlab.com/ce/incoming_email/README.html)
- [Environment variables](https://docs.gitlab.com/omnibus/settings/environment-variables.html)
- [gitlab.yml](https://docs.gitlab.com/omnibus/settings/gitlab.yml.html)
- [Backups](https://docs.gitlab.com/omnibus/settings/backups.html)
- [Pages](https://docs.gitlab.com/ce/pages/administration.html)
- [SSL](https://docs.gitlab.com/omnibus/settings/ssl.html)
- [GitLab and Registry](https://docs.gitlab.com/omnibus/architecture/registry/README.html)

## Updating

- [Upgrade support policy](https://docs.gitlab.com/ee/policy/maintenance.html)
- [Upgrade from Community Edition to Enterprise Edition](https://docs.gitlab.com/omnibus/update/README.html#updating-community-edition-to-enterprise-edition)
- [Updating to the latest version](https://docs.gitlab.com/omnibus/update/README.html#updating-using-the-official-repositories)
- [Downgrading to an earlier version](https://docs.gitlab.com/omnibus/update/README.html#downgrading)
- [Upgrading from a non-Omnibus installation to an Omnibus installation using a backup](https://docs.gitlab.com/omnibus/update/convert_to_omnibus.html#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-using-a-backup)
- [Upgrading from non-Omnibus PostgreSQL to an Omnibus installation in-place](https://docs.gitlab.com/omnibus/update/convert_to_omnibus.html#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-in-place)
- [Upgrading from non-Omnibus MySQL to an Omnibus installation (version 6.8+)](https://docs.gitlab.com/omnibus/update/convert_to_omnibus.html#upgrading-from-non-omnibus-mysql-to-an-omnibus-installation-version-6-8)
- [Updating from GitLab 6.6 and higher to 7.10 or newer](https://docs.gitlab.com/omnibus/update/README.html#updating-from-gitlab-6-6-and-higher-to-7-10-or-newer)
- [Updating from GitLab 6.6.0.pre1 to 6.6.4](https://docs.gitlab.com/omnibus/update/README.html#updating-from-gitlab-6-6-0-pre1-to-6-6-4)
- [Updating from GitLab CI version prior to 5.4.0 to the latest version](https://docs.gitlab.com/omnibus/update/README.html#updating-gitlab-ci-from-prior-5-4-0-to-version-7-14-via-omnibus-gitlab)

## Troubleshooting

- [Hash Sum mismatch when installing packages](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#hash-sum-mismatch-when-installing-packages)
- [Apt error: ‘The requested URL returned error: 403’](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#apt-error-the-requested-url-returned-error-403).
- [GitLab is unreachable in my browser](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#gitlab-is-unreachable-in-my-browser).
- [Emails are not being delivered](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#emails-are-not-being-delivered).
- [Reconfigure freezes at ruby_block "supervise_redis_sleep\" action run](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#reconfigure-freezes-at-ruby_blocksupervise_redis_sleep-action-run).
- [TCP ports for GitLab services are already taken](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#tcp-ports-for-gitlab-services-are-already-taken).
- [Git SSH access stops working on SELinux-enabled systems](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#git-ssh-access-stops-working-on-selinux-enabled-systems).
- [Postgres error ‘FATAL: could not create shared memory segment: Cannot allocate memory’](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#postgres-error-fatal-could-not-create-shared-memory-segment-cannot-allocate-memory).
- [Reconfigure complains about the GLIBC version](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#reconfigure-complains-about-the-glibc-version).
- [Reconfigure fails to create the git user](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#reconfigure-fails-to-create-the-git-user).
- [Failed to modify kernel parameters with sysctl](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#failed-to-modify-kernel-parameters-with-sysctl).
- [I am unable to install omnibus-gitlab without root access](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#i-am-unable-to-install-omnibus-gitlab-without-root-access).
- [gitlab-rake assets:precompile fails with ‘Permission denied’](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#gitlab-rake-assetsprecompile-fails-with-permission-denied).
- [‘Short read or OOM loading DB’ error](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#short-read-or-oom-loading-db-error).
- [‘pg_dump: aborting because of server version mismatch’](https://docs.gitlab.com/omnibus/settings/database.html#using-a-non-packaged-postgresql-database-management-server)
- [‘Errno::ENOMEM: Cannot allocate memory’ during backup or upgrade](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#errnoenomem-cannot-allocate-memory-during-backup-or-upgrade)
- [NGINX error: ‘could not build server_names_hash’](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#nginx-error-could-not-build-server_names_hash-you-should-increase-server_names_hash_bucket_size)
- [Reconfigure fails due to “‘root’ cannot chown” with NFS root_squash](https://docs.gitlab.com/omnibus/common_installation_problems/README.html#reconfigure-fails-due-to-root-cannot-chown-with-nfs-root_squash)

# Maintenance commands

## After installation

### Get service status

Run `sudo gitlab-ctl status`; the output should look like this:

```
run: nginx: (pid 972) 7s; run: log: (pid 971) 7s
run: postgresql: (pid 962) 7s; run: log: (pid 959) 7s
run: redis: (pid 964) 7s; run: log: (pid 963) 7s
run: sidekiq: (pid 967) 7s; run: log: (pid 966) 7s
run: unicorn: (pid 961) 7s; run: log: (pid 960) 7s
```

### Tail process logs

See [settings/logs.md.](https://docs.gitlab.com/omnibus/settings/logs.html)

### Starting and stopping

After omnibus-gitlab is installed and configured, your server will have a Runit service directory (`runsvdir`) process running that gets started at boot via `/etc/inittab` or the `/etc/init/gitlab-runsvdir.conf` Upstart resource. You should not have to deal with the `runsvdir` process directly; you can use the `gitlab-ctl` front-end instead.

You can start, stop or restart GitLab and all of its components with the following commands.

```
# Start all GitLab components
sudo gitlab-ctl start

# Stop all GitLab components
sudo gitlab-ctl stop

# Restart all GitLab components
sudo gitlab-ctl restart
```

Note that on a single-core server it may take up to a minute to restart Unicorn and Sidekiq. Your GitLab instance will give a 502 error until Unicorn is up again.

It is also possible to start, stop or restart individual components.

```
sudo gitlab-ctl restart sidekiq
```

Unicorn supports zero-downtime reloads. These can be triggered as follows:

```
sudo gitlab-ctl hup unicorn
```

Note that you cannot use a Unicorn reload to update the Ruby runtime.

### Invoking Rake tasks

To invoke a GitLab Rake task, use `gitlab-rake`. For example:

```
sudo gitlab-rake gitlab:check
```

Leave out ‘sudo’ if you are the ‘git’ user.

Contrary to with a traditional GitLab installation, there is no need to change the user or the `RAILS_ENV` environment variable; this is taken care of by the `gitlab-rake` wrapper script.

### Starting a Rails console session

If you need access to a Rails production console for your GitLab installation you can start one with the command below. Please be warned that it is very easy to inadvertently modify, corrupt or destroy data from the console.

```
# start a Rails console for GitLab
sudo gitlab-rails console
```

This will only work after you have run `gitlab-ctl reconfigure` at least once.

### Starting a Postgres superuser psql session

If you need superuser access to the bundled Postgres service you can use the `gitlab-psql` command. It takes the same arguments as the regular `psql` command.

```
# Superuser psql access to GitLab's database
sudo gitlab-psql -d gitlabhq_production
```

This will only work after you have run `gitlab-ctl reconfigure` at least once. The `gitlab-psql` command cannot be used to connect to a remote Postgres server, nor to connect to a local non-Omnibus Postgres server.

#### Starting a Postgres superuser psql session in Geo tracking database

Similar to the previous command, if you need superuser access to the bundled Geo tracking database (`geo-postgresql`), you can use the `gitlab-geo-psql`. It takes the same arguments as the regular `psql` command.

```
# Superuser psql access to GitLab's Geo tracking database
sudo gitlab-geo-psql -d gitlabhq_geo_production
```

### Container registry garbage collection

Container registry can use considerable amounts of disk space. To clear up some unused layers, registry includes a garbage collect command.

There are a couple of considerations you need to note before running the built in command:

- The built in command will stop the registry before it starts garbage collect
- The garbage collect command takes some time to complete, depending on the amount of data that exists
- If you changed the location of registry configuration file, you will need to specify the path
- After the garbage collect is done, registry should start up automatically

**Warning** The command below will cause Container registry downtime.

If you did not change the default location of the configuration file, to do garbage collection:

```
sudo gitlab-ctl registry-garbage-collect
```

This command will take some time to complete, depending on the amount of layers you have stored.

If you changed the location of the Container registry config.yml:

```
sudo gitlab-ctl registry-garbage-collect /path/to/config.yml
```

#### Doing garbage collect without downtime

You can do a garbage collect without stopping the Container registry by setting it into a read only mode. During this time, you will be able to pull from the Container registry but you will not be able to push.

These are the steps you need to take in order to complete the garbage collection:

In `/etc/gitlab/gitlab.rb` specify the read only mode:

```
registry['storage'] = {
  'maintenance' => {
    'readonly' => {
      'enabled' => true
    }
  }
}
```

Save and run `sudo gitlab-ctl reconfigure`. This will set the Container registry into the read only mode.

Next, trigger the garbage collect command:

```
sudo /opt/gitlab/embedded/bin/registry garbage-collect /var/opt/gitlab/registry/config.yml
```

This will start the garbage collection. The command will take some time to complete.

Once done, in `/etc/gitlab/gitlab.rb` change the configuration to:

```
registry['storage'] = {
  'maintenance' => {
    'readonly' => {
      'enabled' => false
    }
  }
}
```

and run `sudo gitlab-ctl reconfigure`.

## 



## Installing and maintaining GitLab

Learn how to install, configure, update, and maintain your GitLab instance.

### Installing GitLab

- Install

  : Requirements, directory structures, and installation methods.

  - [Database load balancing](https://docs.gitlab.com/ee/administration/database_load_balancing.html): Distribute database queries among multiple database servers. 
  - [Omnibus support for external MySQL DB](https://docs.gitlab.com/omnibus/settings/database.html#using-a-mysql-database-management-server-enterprise-edition-only): Omnibus package supports configuring an external MySQL database. 
  - [Omnibus support for log forwarding](https://docs.gitlab.com/omnibus/settings/logs.html#udp-log-shipping-gitlab-enterprise-edition-only) 

- High Availability

  : Configure multiple servers for scaling or high availability.

  - [High Availability on AWS](https://docs.gitlab.com/ee/university/high-availability/aws/README.html): Set up GitLab HA on Amazon AWS.

- [Geo](https://docs.gitlab.com/ee/administration/geo/replication/index.html): Replicate your GitLab instance to other geographical locations as a read-only fully operational version. 

- [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html): Quickly fail-over to a different site with minimal effort in a disaster situation. 

- [Pivotal Tile](https://docs.gitlab.com/ee/install/pivotal/index.html): Deploy GitLab as a pre-configured appliance using Ops Manager (BOSH) for Pivotal Cloud Foundry. 

### Configuring GitLab

- [Adjust your instance’s timezone](https://docs.gitlab.com/ee/workflow/timezone.html): Customize the default time zone of GitLab.
- [System hooks](https://docs.gitlab.com/ee/system_hooks/system_hooks.html): Notifications when users, projects and keys are changed.
- [Security](https://docs.gitlab.com/ee/security/README.html): Learn what you can do to further secure your GitLab instance.
- [Usage statistics, version check, and usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html): Enable or disable information about your instance to be sent to GitLab, Inc.
- [Polling](https://docs.gitlab.com/ee/administration/polling.html): Configure how often the GitLab UI polls for updates.
- [GitLab Pages configuration](https://docs.gitlab.com/ee/administration/pages/index.html): Enable and configure GitLab Pages.
- [GitLab Pages configuration for GitLab source installations](https://docs.gitlab.com/ee/administration/pages/source.html): Enable and configure GitLab Pages on
- [Uploads configuration](https://docs.gitlab.com/ee/administration/uploads.html): Configure GitLab uploads storage. [source installations](https://docs.gitlab.com/ee/install/installation.html#installation-from-source).
- [Environment variables](https://docs.gitlab.com/ee/administration/environment_variables.html): Supported environment variables that can be used to override their defaults values in order to configure GitLab.
- [Plugins](https://docs.gitlab.com/ee/administration/plugins.html): With custom plugins, GitLab administrators can introduce custom integrations without modifying GitLab’s source code.
- [Enforcing Terms of Service](https://docs.gitlab.com/ee/user/admin_area/settings/terms.html)
- [Third party offers](https://docs.gitlab.com/ee/user/admin_area/settings/third_party_offers.html)
- [Compliance](https://docs.gitlab.com/ee/administration/compliance.html): A collection of features from across the application that you may configure to help ensure that your GitLab instance and DevOps workflow meet compliance standards.
- [Diff limits](https://docs.gitlab.com/ee/user/admin_area/diff_limits.html): Configure the diff rendering size limits of branch comparison pages.
- [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html): Enable Elasticsearch to empower GitLab’s Advanced Global Search. Useful when you deal with a huge amount of data. 
- [External Classification Policy Authorization](https://docs.gitlab.com/ee/user/admin_area/settings/external_authorization.html) 
- [Upload a license](https://docs.gitlab.com/ee/user/admin_area/license.html): Upload a license to unlock features that are in paid tiers of GitLab. 

#### Customizing GitLab’s appearance

- [Header logo](https://docs.gitlab.com/ee/customization/branded_page_and_email_header.html): Change the logo on all pages and email headers.
- [Favicon](https://docs.gitlab.com/ee/customization/favicon.html): Change the default favicon to your own logo.
- [Branded login page](https://docs.gitlab.com/ee/customization/branded_login_page.html): Customize the login page with your own logo, title, and description.
- [Welcome message](https://docs.gitlab.com/ee/customization/welcome_message.html): Add a custom welcome message to the sign-in page.
- [“New Project” page](https://docs.gitlab.com/ee/customization/new_project_page.html): Customize the text to be displayed on the page that opens whenever your users create a new project.
- **(Premium)** [Additional email text](https://docs.gitlab.com/ee/user/admin_area/settings/email.html): Set a custom message that appears at the bottom of every email.

### Maintaining GitLab

- Raketasks

  : Perform various tasks for maintenance, backups, automatic webhooks setup, etc.

  - [Backup and restore](https://docs.gitlab.com/ee/raketasks/backup_restore.html): Backup and restore your GitLab instance.

- [Operations](https://docs.gitlab.com/ee/administration/operations/index.html): Keeping GitLab up and running (clean up Redis sessions, moving repositories, Sidekiq MemoryKiller, Unicorn).

- [Restart GitLab](https://docs.gitlab.com/ee/administration/restart_gitlab.html): Learn how to restart GitLab and its components.

#### Updating GitLab

- [GitLab versions and maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html): Understand GitLab versions and releases (Major, Minor, Patch, Security), as well as update recommendations.
- [Update GitLab](https://docs.gitlab.com/ee/update/README.html): Update guides to upgrade your installation to a new version.
- [Downtimeless updates](https://docs.gitlab.com/ee/update/README.html#upgrading-without-downtime): Upgrade to a newer major, minor, or patch version of GitLab without taking your GitLab instance offline.
- [Migrate your GitLab CI/CD data to another version of GitLab](https://docs.gitlab.com/ee/migrate_ci_to_ce/README.html): If you have an old GitLab installation (older than 8.0), follow this guide to migrate your existing GitLab CI/CD data to another version of GitLab.

### Upgrading or downgrading GitLab

- [Upgrade from GitLab CE to GitLab EE](https://docs.gitlab.com/ee/update/README.html#upgrading-between-editions): learn how to upgrade GitLab Community Edition to GitLab Enterprise Editions.
- [Downgrade from GitLab EE to GitLab CE](https://docs.gitlab.com/ee/downgrade_ee_to_ce/README.html): Learn how to downgrade GitLab Enterprise Editions to Community Edition.

### GitLab platform integrations

- [Mattermost](https://docs.gitlab.com/omnibus/gitlab-mattermost/): Integrate with [Mattermost](https://about.mattermost.com/), an open source, private cloud workplace for web messaging.
- [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html): Create simple diagrams in AsciiDoc and Markdown documents created in snippets, wikis, and repos.
- [Web terminals](https://docs.gitlab.com/ee/administration/integration/terminal.html): Provide terminal access to your applications deployed to Kubernetes from within GitLab’s CI/CD [environments](https://docs.gitlab.com/ee/ci/environments.html#web-terminals).

## User settings and permissions

- [Libravatar](https://docs.gitlab.com/ee/customization/libravatar.html): Use Libravatar instead of Gravatar for user avatars.

- [Sign-up restrictions](https://docs.gitlab.com/ee/user/admin_area/settings/sign_up_restrictions.html): block email addresses of specific domains, or whitelist only specific domains.

- [Access restrictions](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html#enabled-git-access-protocols): Define which Git access protocols can be used to talk to GitLab (SSH, HTTP, HTTPS).

- Authentication/Authorization

  : Enforce 2FA, configure external authentication with LDAP, SAML, CAS and additional Omniauth providers.

  - [Sync LDAP](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html) 
  - [Kerberos authentication](https://docs.gitlab.com/ee/integration/kerberos.html) 

- [Email users](https://docs.gitlab.com/ee/tools/email.html): Email GitLab users from within GitLab. 

- [User Cohorts](https://docs.gitlab.com/ee/user/admin_area/user_cohorts.html): Display the monthly cohorts of new users and their activities over time.

- [Audit logs and events](https://docs.gitlab.com/ee/administration/audit_events.html): View the changes made within the GitLab server. 

- [Auditor users](https://docs.gitlab.com/ee/administration/auditor_users.html): Users with read-only access to all projects, groups, and other resources on the GitLab instance.

  - [Postfix for incoming email](https://docs.gitlab.com/ee/administration/reply_by_email_postfix_setup.html): Set up a basic Postfix mail server with IMAP authentication on Ubuntu for incoming emails.

- **(Premium)** [Additional custom email text](https://docs.gitlab.com/ee/user/admin_area/settings/email.html): Set a custom message that appears at the bottom of the every email.

## Project settings

- [Container Registry](https://docs.gitlab.com/ee/administration/container_registry.html): Configure Container Registry with GitLab.
- [Issue closing pattern](https://docs.gitlab.com/ee/administration/issue_closing_pattern.html): Customize how to close an issue from commit messages.
- [Gitaly](https://docs.gitlab.com/ee/administration/gitaly/index.html): Configuring Gitaly, GitLab’s Git repository storage service.
- [Default labels](https://docs.gitlab.com/ee/user/admin_area/labels.html): Create labels that will be automatically added to every new project.
- [Restrict the use of public or internal projects](https://docs.gitlab.com/ee/public_access/public_access.html#restricting-the-use-of-public-or-internal-projects): Restrict the use of visibility levels for users when they create a project or a snippet.
- [Custom project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html): Configure a set of projects to be used as custom templates when creating a new project. 
- [Maven Repository](https://docs.gitlab.com/ee/administration/maven_repository.html): Enable Maven Repository within GitLab.

### Repository settings

- [Repository checks](https://docs.gitlab.com/ee/administration/repository_checks.html): Periodic Git repository checks.
- [Repository storage paths](https://docs.gitlab.com/ee/administration/repository_storage_paths.html): Manage the paths used to store repositories.
- [Repository storage rake tasks](https://docs.gitlab.com/ee/administration/raketasks/storage.html): A collection of rake tasks to list and migrate existing projects and attachments associated with it from Legacy storage to Hashed storage.
- [Limit repository size](https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html): Set a hard limit for your repositories’ size. 

## Continuous Integration settings

- [Enable/disable GitLab CI/CD](https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#site-wide-admin-setting): Enable or disable GitLab CI/CD for your instance.
- [GitLab CI/CD admin settings](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html): Enable or disable Auto DevOps site-wide and define the artifacts’ max size and expiration time.
- [Job artifacts](https://docs.gitlab.com/ee/administration/job_artifacts.html): Enable, disable, and configure job artifacts (a set of files and directories which are outputted by a job when it completes successfully).
- [Job traces](https://docs.gitlab.com/ee/administration/job_traces.html): Information about the job traces (logs).
- [Register Shared and specific Runners](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-shared-runner): Learn how to register and configure Shared and specific Runners to your own instance.
- [Shared Runners pipelines quota](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#shared-runners-pipeline-minutes-quota): Limit the usage of pipeline minutes for Shared Runners.
- [Enable/disable Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html#enabling-auto-devops): Enable or disable Auto DevOps for your instance.

## Git configuration options

- [Custom Git hooks](https://docs.gitlab.com/ee/administration/custom_hooks.html): Custom Git hooks (on the filesystem) for when webhooks aren’t enough.
- [Git LFS configuration](https://docs.gitlab.com/ee/workflow/lfs/lfs_administration.html): Learn how to configure LFS for GitLab.
- [Housekeeping](https://docs.gitlab.com/ee/administration/housekeeping.html): Keep your Git repositories tidy and fast.
- [Configuring Git Protocol v2](https://docs.gitlab.com/ee/administration/git_protocol.html): Git protocol version 2 support.

## Monitoring GitLab

- Monitoring GitLab

  :

  - [Monitoring uptime](https://docs.gitlab.com/ee/user/admin_area/monitoring/health_check.html): Check the server status using the health check endpoint.
  - [IP whitelist](https://docs.gitlab.com/ee/administration/monitoring/ip_whitelist.html): Monitor endpoints that provide health check information when probed.
  - [Monitoring GitHub imports](https://docs.gitlab.com/ee/administration/monitoring/github_imports.html): GitLab’s GitHub Importer displays Prometheus metrics to monitor the health and progress of the importer.

### Performance Monitoring

- GitLab Performance Monitoring

  :

  - [Enable Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/gitlab_configuration.html): Enable GitLab Performance Monitoring.
  - [GitLab performance monitoring with InfluxDB](https://docs.gitlab.com/ee/administration/monitoring/performance/influxdb_configuration.html): Configure GitLab and InfluxDB for measuring performance metrics.
  - [InfluxDB Schema](https://docs.gitlab.com/ee/administration/monitoring/performance/influxdb_schema.html): Measurements stored in InfluxDB.
  - [GitLab performance monitoring with Prometheus](https://docs.gitlab.com/ee/administration/monitoring/prometheus/index.html): Configure GitLab and Prometheus for measuring performance metrics.
  - [GitLab performance monitoring with Grafana](https://docs.gitlab.com/ee/administration/monitoring/performance/grafana_configuration.html): Configure GitLab to visualize time series metrics through graphs and dashboards.
  - [Request Profiling](https://docs.gitlab.com/ee/administration/monitoring/performance/request_profiling.html): Get a detailed profile on slow requests.
  - [Performance Bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html): Get performance information for the current page.

## Analytics

- [Pseudonymizer](https://docs.gitlab.com/ee/administration/pseudonymizer.html): Export data from GitLab’s database to CSV files in a secure way.

## Troubleshooting

- [Debugging tips](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html): Tips to debug problems when things go wrong
- [Log system](https://docs.gitlab.com/ee/administration/logs.html): Where to look for logs.
- [Sidekiq Troubleshooting](https://docs.gitlab.com/ee/administration/troubleshooting/sidekiq.html): Debug when Sidekiq appears hung and is not processing jobs.

## ---

sudo gitlab-ctl status

# Start all GitLab components

sudo gitlab-ctl start

# Stop all GitLab components

sudo gitlab-ctl stop

# Restart all GitLab components

sudo gitlab-ctl restart

sudo gitlab-ctl restart sidekiq

sudo gitlab-ctl hup unicorn
sudo gitlab-rake gitlab:check

# start a Rails console for GitLab

sudo gitlab-rails console

# Superuser psql access to GitLab's database

sudo gitlab-psql -d gitlabhq_production

# Superuser psql access to GitLab's Geo tracking database

sudo gitlab-geo-psql -d gitlabhq_geo_production

#Container registry garbage collection
sudo gitlab-ctl registry-garbage-collect

https://docs.gitlab.com/omnibus/maintenance/
https://docs.gitlab.com/omnibus/settings/logs.html
https://docs.gitlab.com/omnibus/settings/ssl.html

http://www.postgresqltutorial.com/postgresql-cheat-sheet/

POSTGRE
\q: Quit/Exit
\c __database__: Connect to a database
\d __table__: Show table definition including triggers
\l: List databases
\dy: List events
\df: List functions
\di: List indexes
\dn: List schemas
\dt *.*: List tables from all schemas (if *.* is omitted will only show SEARCH_PATH ones)
\x: Pretty-format query results instead of the not-so-useful ASCII tables
\copy (SELECT * FROM __table_name__) TO 'file_path_and_name.csv' WITH CSV: Export a table as CSV

SELECT * FROM weather;

SELECT city, temp_lo, temp_hi, prcp, date FROM weather;



https://www.postgresql.org/docs/9.1/tutorial-select.html

https://chartio.com/resources/tutorials/how-to-list-databases-and-tables-in-postgresql-using-psql/

https://docs.gitlab.com/omnibus/settings/README.html

https://docs.gitlab.com/ee/ssh/README.html#rsa-ssh-keys

https://docs.gitlab.com/ee/user/

------



**Describe the resources or steps you may use to resolve a customer’s problem when you don’t know the answer. (Make assumptions wherever you need to about how the GitLab team works).**

Resolving a technical problem as a Support Engineer is like diagnosing a physiological problem as a medical doctor. People contact Doctors and Support Engineers with a problem and a list of symptoms.  The ability to diagnose the cause of the problem usually requires two things - excellent communication AND careful examination.
Sometimes a person may come to a doctor with a problem that has a cause and a solution which is easy to infer (ex: dislocated joint, broken bone).  In the same way, people sometimes alert Support Engineers to problems they can quickly identify and fix (ex: missing software dependency, formatting error in a configuration file).  These emergencies are the easiest to diagnose, but they are relatively uncommon.
It is much more difficult to diagnose a problem if the symptoms are common to many ailments or technical issues. As a Support Engineer, a client reporting “slowness” or “performance issues” can be like a medical patient complaining of “headache” or “not feeling well”. An accurate diagnosis requires both thorough investigation AND excellent communication. The ability to communicate and understand, to ask the right questions, and to get the answers necessary to diagnose the problem, is just as essential as a thorough examination (technical or physical) by a trained expert.
To answer the question more directly:
RESOURCES
The resources I use to resolve a customers problem might include:
•	Official Internal Documentation (resources intended for employees: Support Team docs or wiki, Troubleshooting guides, Internal Developer Documentation)
•	Official Product Documentation (any PUBLIC support resources (ex: end-user documentation, developer guides, FAQ, installation instructions for Admins, troubleshooting guides, etc.))
•	Chat/Email with teammates and coworkers
•	Support Ticketing system and its archive of resolved support tickets
•	Targeted Google searches using advanced search modifiers to eliminate irrelevant search results
•	Official and Respected Support Forums (StackOverflow, official Distribution or Software support forums)
•	Official Support IRC channels (great resource quick answers to general Linux and open source software questions and support)
•	Any relevant published materials from respected publishers (O'Reilly, Packt, relevant Certification Study Guides)
TECHNICAL TROUBLESHOOTING STEPS I WOULD TAKE
In a situation like this I would:
•	Carefully listen to or read the issue being reported and make a note of any subtle details that might help to eliminate potential causes or diagnose the cause. 
•	Analyze the problem and issues being reported, while also making sure to let the person know they
•	Consider potential root causes that might cause this, and identify any relevant or important variables that are currently unknown.
•	Identify what information is needed to duplicate and or diagnose the issue. In situations where the cause is difficult to infer, I respond to the individual reporting the problem with questions to help clarify and identify the root cause. This is often necessary.
Then I try to determine WHEN the problem is occurring? 
•	when was the problem first detected?
•	when did it start and/or stop?
•	events leading up to and immediately before the problem
•	how frequently the problem occurs
•	how long the problem endures
Using this knowledge, and based on whether it is a single complaint or many, I can usually narrow-in on WHERE the problem is occurring. If its a problem I'm familiar with, or the team has encountered before, I can consult past solutions.
Some quick and easy checks can help identify the course in situations where many users are affected.
If I cannot explain why an issue is happening, I'll often try to duplicate the issue on my end and perform some tests to investigate the most-likely suspects.